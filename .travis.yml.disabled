language: java
os: linux
dist: focal
services: docker

cache:
  directories:
    - ${HOME}/.m2

env:
  global:
    - MAVEN_PROPS="-Djavadoc.path=`which javadoc` -Dmaven.wagon.http.retryHandler.count=5 -Dmaven.wagon.httpconnectionManager.ttlSeconds=25"
    - SETTINGS=".travis-ci/settings.xml"
    - PROFILES="gpg,release-sign-artifacts,sonatype-deployment,sonatype-snapshots,sonatype-staging,sonatype-releases"

install:
  - source .travis-ci/install-gpg.sh
  - source .travis-ci/install-zulu${JAVA_VERSION}.sh
  - source .travis-ci/install-maven.sh

script:
  - mvn -U -V -s "${SETTINGS}" -P${PROFILES} ${MAVEN_PROPS} dependency:list-repositories dependency:tree help:active-profiles clean install site site:stage

jobs:
  include:
  - stage: mvn build
    env: JAVA_VERSION=8
  - stage: mvn build
    env: JAVA_VERSION=11
  - stage: mvn build
    dist: xenial
    env: JAVA_VERSION=8
  - stage: mvn build
    dist: xenial
    env: JAVA_VERSION=11
  - stage: docker mvn build
    env: CBD="/usr/src/build" P=luminositylabs-oss I=luminositylabs/maven T=3.6.3_openjdk-11.0.9.1_zulu-alpine-11.43.55
    install: source .travis-ci/install-gpg.sh
    script:
    - docker container run --rm -it -v "$(pwd)":"${CBD}" -v ${HOME}/.gnupg:/root/.gnupg -v ${P}-${T}-mvn-repo:/root/.m2 -w "${CBD}" ${I}:${T} mvn -U -V -s "${SETTINGS}" -P${PROFILES} ${MAVEN_PROPS} dependency:list-repositories dependency:tree help:active-profiles clean install site site:stage
  - stage: mvn deploy
    if: branch in (main) AND type not in (pull_request)
    dist: focal
    env: JAVA_VERSION=11
    script:
      - mvn -U -V -s "${SETTINGS}" -P${PROFILES} ${MAVEN_PROPS} deploy
